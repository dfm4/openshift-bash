#!/bin/bash
which jq > /dev/null
if [ $? -gt 0 ]
then
  echo "install jq https://stedolan.github.io/jq/" >&2
  exit 1
fi

if [ -z ${DDS_URL} ]
then
  echo "DDS_URL environment required" >&2
  exit 1
fi
if [ -z ${AGENT_KEY} ]
then
  echo "AGENT_KEY environment required" >&2
  exit 1
fi
if [ -z ${USER_KEY} ]
then
  echo "USER_KEY environment required" >&2
  exit 1
fi

project_id=${PROJECT_ID}
if [ -z ${project_id} ]
then
  echo "PROJECT_ID environment required" >&2
  exit 1
fi

desired_files=${FILES_DESIRED}
if [ -z ${desired_files} ]
then
desired_files=300
fi

check_for_errors() {
  if [ $? -gt 0 ]
  then
    echo "Curl Problem! ${resp}" >&2
    exit 1
  fi
  error=`echo "${resp}" | jq '.error'`
  if [ "${error}" != "null" ]
  then
    echo "API Problem! ${error}" >&2
    exit 1
  fi
}

# Authenticate
resp=`curl -f -s -S -X POST --header "Content-Type: application/json" --header "Accept: application/json" -d '{"agent_key":"'${AGENT_KEY}'","user_key":"'${USER_KEY}'"}' "${DDS_URL}/api/v1/software_agents/api_token"`
check_for_errors
auth_token=`echo "${resp}" | jq -r '.api_token'`

original_filename=`uuidgen`
upload_size=1
upload_md5='cf23df2207d99a74fbe169e3eba035e633b65d94'

for file_number in $(seq 1 ${desired_files})
do
  # Create Upload
  resp=`curl -f -s -S -X POST --header "Content-Type: application/json" --header "Accept: application/json" --header "Authorization: ${auth_token}" -d '{"name":"'${original_filename}-${i}'","content_type":"audio%2Fmpeg","size":"'${upload_size}'","hash":{"value":"'${upload_md5}'","algorithm":"md5"}}' "${DDS_URL}/api/v1/projects/${project_id}/uploads"`
  check_for_errors
  upload_id=`echo "${resp}" | tail -1 | jq -r '.id'`

  # Create Chunk
  number=1
  resp=`curl -f -s -S -X PUT --header "Content-Type: application/json" --header "Accept: application/json" --header "Authorization: ${auth_token}" -d '{"number":"'${number}'","size":"'${upload_size}'","hash":{"value":"'${upload_md5}'","algorithm":"md5"}}' "${DDS_URL}/api/v1/uploads/${upload_id}/chunks"`
  check_for_errors

  # Complete Upload
  resp=`curl -f -s -S -X PUT --header "Content-Type: application/json" --header "Accept: application/json" --header "Authorization: ${auth_token}" -d '{"hash":{"value":"'${upload_md5}'","algorithm":"md5"}}' "${DDS_URL}/api/v1/uploads/${upload_id}/complete"`
  check_for_errors

  # Create File
  resp=`curl -f -s -S -X POST --header "Content-Type: application/json" --header "Accept: application/json" --header "Authorization: ${auth_token}" -d '{"parent":{"kind":"dds-project","id":"'${project_id}'"},"upload":{"id":"'${upload_id}'"}}' "${DDS_URL}/api/v1/files"`
  check_for_errors

  echo -n .
done
echo done.
